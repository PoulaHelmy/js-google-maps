// const APIKEY= "AIzaSyBqNzTcMULRgUpe93sR2oVTQCpwhHqnIfU";
// const markers = [];
let map;
const state = {
    APIKEY: "AIzaSyBqNzTcMULRgUpe93sR2oVTQCpwhHqnIfU",
    markers: [],
    // map: '',
    placeMarkers: [],
    // ploygon: null,
    // ALL Styles
    // styles: [
    //     {
    //         featureType: 'water',
    //         stylers: [
    //             {color: '#19a0d8'}
    //         ]
    //     }, {
    //         featureType: 'administrative',
    //         elementType: 'labels.text.stroke',
    //         stylers: [
    //             {color: '#ffffff'},
    //             {weight: 6}
    //         ]
    //     }, {
    //         featureType: 'administrative',
    //         elementType: 'labels.text.fill',
    //         stylers: [
    //             {color: '#e85113'}
    //         ]
    //     }, {
    //         featureType: 'road.highway',
    //         elementType: 'geometry.stroke',
    //         stylers: [
    //             {color: '#efe9e4'},
    //             {lightness: -40}
    //         ]
    //     }, {
    //         featureType: 'transit.station',
    //         stylers: [
    //             {weight: 9},
    //             {hue: '#e85113'}
    //         ]
    //     }, {
    //         featureType: 'road.highway',
    //         elementType: 'labels.icon',
    //         stylers: [
    //             {visibility: 'off'}
    //         ]
    //     }, {
    //         featureType: 'water',
    //         elementType: 'labels.text.stroke',
    //         stylers: [
    //             {lightness: 100}
    //         ]
    //     }, {
    //         featureType: 'water',
    //         elementType: 'labels.text.fill',
    //         stylers: [
    //             {lightness: -100}
    //         ]
    //     }, {
    //         featureType: 'poi',
    //         elementType: 'geometry',
    //         stylers: [
    //             {visibility: 'on'},
    //             {color: '#f0e4d3'}
    //         ]
    //     }, {
    //         featureType: 'road.highway',
    //         elementType: 'geometry.fill',
    //         stylers: [
    //             {color: '#efe9e4'},
    //             {lightness: -25}
    //         ]
    //     }
    // ],
    // ALL Locations
    // locations: getAllLocation()
}

// let directionsService;

function initMap() {
    const directionsRenderer = new google.maps.DirectionsRenderer();
    map = new google.maps.Map(
        document.getElementById('map'), {
            zoom: 15,
            center: {lat: 30.033333, lng: 31.233334},
            // styles: state.styles,
            mapTypeControl: true
        });
    directionsRenderer.setMap(map);
    // Create a startLocation in order to execute a places search
    const startLocation = new google.maps.places.SearchBox(
        document.getElementById('places-search-start'));
    startLocation.setBounds(map.getBounds());
    startLocation.addListener('places_changed', function () {
        searchBoxPlacesWithoutHiding(this);
    });
    // Create endLocation in order to execute a places search
    const endLocation = new google.maps.places.SearchBox(
        document.getElementById('places-search-end'));
    endLocation.setBounds(map.getBounds());
    endLocation.addListener('places_changed', function () {
        searchBoxPlacesWithoutHiding(this);
    });

    document.getElementById('draw-routes').addEventListener('click', function () {
        const start = document.getElementById('places-search-start').value;
        const end = document.getElementById('places-search-end').value;
        console.log(start, end)
        let directionsService = new google.maps.DirectionsService();
        const request = {
            origin: start,
            destination: end,
            travelMode: 'DRIVING'
        };
        directionsService.route(request, function (result, status) {
            if (status == 'OK') {
                directionsRenderer.setDirections(result);
            }
        });
    });
    document.getElementById('calc-distance').addEventListener('click', function () {
        const start = document.getElementById('places-search-start').value;
        const end = document.getElementById('places-search-end').value;
        console.log(start, end)
        let service = new google.maps.DistanceMatrixService();
        service.getDistanceMatrix(
            {
                origins: [start],
                destinations: [end],
                travelMode: 'DRIVING',
                unitSystem: google.maps.UnitSystem.METRIC,
                // transitOptions: TransitOptions,
                // drivingOptions: DrivingOptions,
                // unitSystem: UnitSystem,
                // avoidHighways: Boolean,
                // avoidTolls: Boolean,
            }, function (response, status) {
                if (status !== google.maps.DistanceMatrixStatus.OK) {
                    window.alert('Error was: ' + status);
                } else {
                    displayMarkersWithinTime(response);
                }
                // if (status === google.maps.DirectionsStatus.OK) {
                //     console.log(response);
                //     let directionsDisplay = new google.maps.DirectionsRenderer({
                //         map: map,
                //         directions: response,
                //         draggable: true,
                //         polylineOptions: {
                //             strokeColor: 'green'
                //         }
                //     });
                // } else {
                //     window.alert('Directions request failed due to ' + status);
                // }
            });


    });
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Create a searchbox in order to execute a places search
    const searchBox = new google.maps.places.SearchBox(
        document.getElementById('places-search-hourly'));
    // Bias the searchbox to within the bounds of the map.
    searchBox.setBounds(map.getBounds());
    // Listen for the event fired when the user selects a prediction from the
    // picklist and retrieve more details for that place.
    searchBox.addListener('places_changed', function () {
        searchBoxPlaces(this);
    });
    document.getElementById('show-listings').addEventListener('click', function () {
        const locations = getAllLocation();
        // Create a "highlighted location" marker color for when the user
        // mouses over the marker.
        var highlightedIcon = makeMarkerIcon('FFFF24');
        let bounds = new google.maps.LatLngBounds();
        locations.forEach((location, index) => {
            let position = location.location;
            let title = location.title;
            let marker = new google.maps.Marker({
                position: position,
                title: title,
                animation: google.maps.Animation.DROP,
                // icon: defaultIcon,
                id: index

            });
            state.markers.push(marker);
            bounds.extend(marker.position);
            marker.addListener('click', function () {
                populateInfoWindow(this, largerInfoWindow);
            });

            map.fitBounds(bounds);
        });

        // Extend the boundaries of the map for each marker and display the marker
        state.markers.forEach((marker) => {
            marker.setMap(map);
            bounds.extend(marker.position)
        })

        map.fitBounds(bounds);

    });
    document.getElementById('hide-listings').addEventListener('click', function () {
        hideMarkers(state.markers);
    });

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}//end of Inint MAP

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// This function will go through each of the results, and,
// if the distance is LESS than the value in the picker, show it on the map.
function displayMarkersWithinTime(response) {
    // var maxDuration = document.getElementById('max-duration').value;
    var origins = response.originAddresses;
    var destinations = response.destinationAddresses;
    // Parse through the results, and get the distance and duration of each.
    // Because there might be  multiple origins and destinations we have a nested loop
    // Then, make sure at least 1 result was found.
    var atLeastOne = false;
    for (var i = 0; i < origins.length; i++) {
        var results = response.rows[i].elements;
        for (var j = 0; j < results.length; j++) {
            var element = results[j];
            if (element.status === "OK") {
                // The distance is returned in feet, but the TEXT is in miles. If we wanted to switch
                // the function to show markers within a user-entered DISTANCE, we would need the
                // value for distance, but for now we only need the text.
                console.log(element.distance);
                var distanceText = element.distance.text;
                // Duration value is given in seconds so we make it MINUTES. We need both the value
                // and the text.
                var duration = element.duration.value / 60;
                var durationText = element.duration.text;
                if (duration) {
                    //the origin [i] should = the markers[i]
                    state.placeMarkers[i].setMap(map);
                    atLeastOne = true;
                    // Create a mini infowindow to open immediately and contain the
                    // distance and duration
                    var infowindow = new google.maps.InfoWindow({
                        content: durationText + ' away, ' + distanceText
                    });
                    infowindow.open(map, state.placeMarkers[i]);
                    // Put this in so that this small window closes if the user clicks
                    // the marker, when the big infowindow opens
                    state.placeMarkers[i].infowindow = infowindow;
                    google.maps.event.addListener(state.placeMarkers[i], 'click', function () {
                        this.infowindow.close();
                    });
                }
            }
        }
    }
    if (!atLeastOne) {
        window.alert('We could not find any locations within that distance!');
    }
}

// This function fires when the user selects a searchbox picklist item.
// It will do a nearby search using the selected query string or place.
function searchBoxPlaces(searchBox) {
    hideMarkers(state.placeMarkers);
    const places = searchBox.getPlaces();
    console.log('places-----------------' + places)
    if (places.length == 0) {
        window.alert('We did not find any places matching that search!');
    } else {
        // For each place, get the icon, name and location.
        hideMarkers(state.placeMarkers);
        hideMarkers(state.markers);
        createMarkersForPlaces(places);
    }
}// This function fires when the user selects a searchbox picklist item.
// It will do a nearby search using the selected query string or place.
function searchBoxPlacesWithoutHiding(searchBox) {

    // hideMarkers(state.placeMarkers);
    const places = searchBox.getPlaces();

    if (places.length == 0) {
        window.alert('We did not find any places matching that search!');
    } else {
        // For each place, get the icon, name and location.
        createMarkersForPlaces(places);
    }
}

// This function creates markers for each place found in either places search.
function createMarkersForPlaces(places) {
    const bounds = new google.maps.LatLngBounds();
    places.forEach((place, index) => {
        const marker = new google.maps.Marker({
            map: map,
            // title: place.name,
            position: place.geometry.location,
            id: place.place_id
        });
        // Create a single infowindow to be used with the place details information
        // so that only one is open at once.
        const placeInfoWindow = new google.maps.InfoWindow();
        // If a marker is clicked, do a place details search on it in the next function.
        marker.addListener('click', function () {
            if (placeInfoWindow.marker == this) {
                console.log("This infowindow already is on this marker!");
            } else {
                getPlacesDetails(this, placeInfoWindow);
            }
        });
        state.placeMarkers.push(marker);
        if (place.geometry.viewport) {
            // Only geocodes have viewport.
            bounds.union(place.geometry.viewport);
        } else {
            bounds.extend(place.geometry.location);
        }
    });
    map.fitBounds(bounds);
}

// This is the PLACE DETAILS search - it's the most detailed so it's only
// executed when a marker is selected, indicating the user wants more
// details about that place.
function getPlacesDetails(marker, infowindow) {
    const service = new google.maps.places.PlacesService(map);
    service.getDetails({
        placeId: marker.id
    }, function (place, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
            // Set the marker property on this infowindow so it isn't created again.
            infowindow.marker = marker;
            const innerHTML = `<div>
                ${place.name ? `<strong>${place.name}</strong>` : ''}</div>`;
            infowindow.setContent(innerHTML);
            infowindow.open(map, marker);
            // Make sure the marker property is cleared if the infowindow is closed.
            infowindow.addListener('closeclick', function () {
                infowindow.marker = null;
            });
        }
    });
}

// This function will loop through the listings and hide them all.
function hideMarkers(markers) {
    markers.forEach((marker) => {
        marker.setMap(null);
    })
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//getAll Locations
function getAllLocation() {
    return [
        // {title: 'Sohag, Sohag Governorate, Egypt', location: {lat: 26.549999, lng: 31.700001}},
        // {title: 'Shubra El Kheima, Qalyubia, Egypt', location: {lat: 30.128611, lng: 31.242222}},
        {title: 'Cairo, Cairo Governorate, Egypt', location: {lat: 30.033333, lng: 31.233334}},
        // {title: 'Sherbeen, Dakahlia Governorate, Egypt', location: {lat: 31.192560, lng: 31.520460}},
        // {title: '6th of October City, Giza Governorate, Egypt', location: {lat: 29.952654, lng: 30.921919}},
        // {title: 'Asyut, Assiut Governorate, Egypt', location: {lat: 27.180134, lng: 31.189283}},
        // {title: 'Mansoura, Dakahlia Governorate, Egypt', location: {lat: 31.037933, lng: 31.381523}},
        // {title: 'Sheikh Zayed City, Giza Governorate, Egypt', location: {lat: 30.056021, lng: 30.976639}},
        {title: 'New Cairo, Cairo, Egypt', location: {lat: 30.005493, lng: 31.477898}},
        // {title: 'Alexandria, Egypt', location: {lat: 31.205753, lng: 29.924526}},
        {title: 'downtown mall, new cairo city, cairo governorate, egypt', location: {lat: 30.0074, lng: 31.4913}},
        {title: 'city stars , egypt', location: {lat: 30.1068, lng: 31.3671}},
        // {title: 'Park Ave Penthouse', location: {lat: 40.7713024, lng: -73.9632393}},
        // {title: 'Chelsea Loft', location: {lat: 40.7444883, lng: -73.9949465}},
        // {title: 'Union Square Open Floor Plan', location: {lat: 40.7347062, lng: -73.9895759}},
        // {title: 'East Village Hip Studio', location: {lat: 40.7281777, lng: -73.984377}},
        // {title: 'TriBeCa Artsy Bachelor Pad', location: {lat: 40.7195264, lng: -74.0089934}},
        // {title: 'Chinatown Homey Space', location: {lat: 40.7180628, lng: -73.9961237}}

    ];
}// end of getAll Locations

// This function populates the infowindow when the marker is clicked. We'll only allow
// one infowindow which will open at the marker that is clicked, and populate based
// on that markers position.
function populateInfoWindow(marker, infowindow) {
    // Check to make sure the infowindow is not already opened on this marker.
    if (infowindow.marker != marker) {
        // Clear the infowindow content to give the streetview time to load.
        infowindow.setContent('');
        infowindow.marker = marker;
        // Make sure the marker property is cleared if the infowindow is closed.
        infowindow.addListener('closeclick', function () {
            infowindow.marker = null;
        });
        // Open the infowindow on the correct marker.
        infowindow.open(map, marker);
    }
}


// This function takes in a COLOR, and then creates a new marker
// icon of that color. The icon will be 21 px wide by 34 high, have an origin
// of 0, 0 and be anchored at 10, 34).
function makeMarkerIcon(markerColor) {
    var markerImage = new google.maps.MarkerImage(
        'http://chart.googleapis.com/chart?chst=d_map_spin&chld=1.15|0|' + markerColor +
        '|40|_|%E2%80%A2',
        new google.maps.Size(21, 34),
        new google.maps.Point(0, 0),
        new google.maps.Point(10, 34),
        new google.maps.Size(21, 34));
    return markerImage;
}


// const marker = new google.maps.Marker({position: {lat: 30.033333, lng: 31.233334}, map: map, title: 'CAIRO'});
// // infoWindow
// const infoWindow = new google.maps.InfoWindow({
//     content: 'Do You ever feel like an infoWindow'
// });
// marker.addListener('click', function () {
//     infoWindow.open(map, marker);
// })
// let largerInfoWindow = new google.maps.InfoWindow();
// Style the markers a bit. This will be our listing marker icon.
// var defaultIcon = makeMarkerIcon('0091ff');

// Create a "highlighted location" marker color for when the user
// mouses over the marker.
// var highlightedIcon = makeMarkerIcon('FFFF24');
// let bounds = new google.maps.LatLngBounds();
// locations.forEach((location, index) => {
//
//     let position = location.location;
//     let title = location.title;
//     let marker = new google.maps.Marker({
//         position: position,
//         title: title,
//         animation: google.maps.Animation.DROP,
//         icon: defaultIcon,
//         id: index
//
//     });
//     markers.push(marker);
//     // bounds.extend(marker.position);
//     // marker.addListener('click', function () {
//     //     populateInfoWindow(this, largerInfoWindow);
//     // });
//     // Two event listeners - one for mouseover, one for mouseout,
//     // to change the colors back and forth.
//     // marker.addListener('mouseover', function () {
//     //     this.setIcon(highlightedIcon);
//     // });
//     // marker.addListener('mouseout', function () {
//     //     this.setIcon(defaultIcon);
//     // });
//     // map.fitBounds(bounds);
// });
// document.getElementById('show-listings').addEventListener('click', showListings);
// document.getElementById('hide-listings').addEventListener('click', hideListings);
